package ru.rencredit.jschool.kuzyushin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.*;

import java.util.List;

@Component
public final class UserListListener extends AbstractListener {

    @NotNull
    private final UserEndpoint userEndpoint;

    @Autowired
    public UserListListener(
            final @NotNull UserEndpoint userEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user list";
    }

    @Override
    @EventListener(condition = "@userListListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LIST USERS]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        @NotNull final List<UserDTO> usersDTO = userEndpoint.findAllUsers(sessionDTO);
        int index = 1;
        for (@NotNull final UserDTO userDTO: usersDTO) {
            System.out.println(index + ". " + userDTO.getId() + ": " + userDTO.getLogin());
            index++;
        }
        System.out.println("[OK]");
    }
}
