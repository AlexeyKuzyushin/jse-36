package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @Autowired
    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void create(final @Nullable String login, final @Nullable String password, final @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        userRepository.save(user);
    }

    @Override
    public void create(final @Nullable String login, final @Nullable String password, final @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.save(user);
    }

    @Override
    @Nullable
    public User getOneById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = userRepository.getOne(id);
        return user;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return UserDTO.toDTO(userRepository.findAll());
    }

    @Override
    @Nullable
    public User findById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<User> user = userRepository.findById(id);
        return user.get();
    }

    @Override
    @Nullable
    public User findByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @Override
    public void updateLogin(final @Nullable String id, final @Nullable String login) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findById(id);
        user.setLogin(login);
        userRepository.save(user);
    }

    @Override
    public void updatePassword(final @Nullable String id, final @Nullable String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(id);
        user.setPasswordHash(password);
        userRepository.save(user);
    }

    @Override
    public void updateEmail(final @Nullable String id, final @Nullable String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(id);
        user.setEmail(email);
        userRepository.save(user);
    }

    @Override
    public void updateFirstName(final @Nullable String id, final @Nullable String firstName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        user.setFirstName(firstName);
        userRepository.save(user);
    }

    @Override
    public void updateLastName(final @Nullable String id, final @Nullable String lastName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        user.setLastName(lastName);
        userRepository.save(user);
    }

    @Override
    public void updateMiddleName(final @Nullable String id, final @Nullable String middleName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        user.setMiddleName(middleName);
        userRepository.save(user);
    }

    @Override
    public void lockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(true);
        userRepository.save(user);
    }

    @Override
    public void unlockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(false);
        userRepository.save(user);
    }

    @Override
    public void load(final @Nullable List<UserDTO> users) {
        if (users == null) return;
        for (final UserDTO userDTO: users){
            @NotNull final User user = new User();
            user.setLogin(userDTO.getLogin());
            user.setFirstName(userDTO.getFirstName());
            user.setLastName(userDTO.getLastName());
            user.setMiddleName(userDTO.getMiddleName());
            user.setId(userDTO.getId());
            user.setPasswordHash(userDTO.getPasswordHash());
            user.setEmail(userDTO.getEmail());
            user.setRole(userDTO.getRole());
            user.setLocked(userDTO.getLocked());
            userRepository.save(user);
        }
    }
}
