package ru.rencredit.jschool.kuzyushin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.*;

import javax.xml.ws.Endpoint;

@Component
public final class ServerBootstrap {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Autowired
    private AnnotationConfigApplicationContext context;

    public void run(){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        initEndpoint();
    }

    private void initEndpoint() {
        registry(context.getBean(UserEndpoint.class));
        registry(context.getBean(SessionEndpoint.class));
        registry(context.getBean(DataEndpoint.class));
        registry(context.getBean(TaskEndpoint.class));
        registry(context.getBean(ProjectEndpoint.class));
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final Integer port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }
}
