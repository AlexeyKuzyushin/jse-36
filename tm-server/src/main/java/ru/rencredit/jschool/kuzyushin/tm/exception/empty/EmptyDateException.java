package ru.rencredit.jschool.kuzyushin.tm.exception.empty;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class EmptyDateException extends AbstractException {

    public EmptyDateException() {
        super("Error! Date is empty...");
    }
}