package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.IUserEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Nullable
    @Autowired
    private IUserService userService;

    public UserEndpoint(final @NotNull ISessionService sessionService) {
        super(sessionService);
    }

    @Override
    public @NotNull List<UserDTO> findAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return userService.findAll();
    }

    @Override
    @WebMethod
    public void createUserWithEmail(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email) {
        userService.create(login, password, email);
    }

    @Override
    @WebMethod
    public void createUserWithRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable final Role role) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        userService.create(login, password, role);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        userService.unlockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        userService.removeById(id);
    }

    @Override
    @WebMethod
    public void updateUserLogin(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable String login) {
        sessionService.validate(sessionDTO);
        userService.updateLogin(sessionDTO.getUserId(), login);
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "password", partName = "password") @Nullable String password) {
        sessionService.validate(sessionDTO);
        userService.updatePassword(sessionDTO.getUserId(), password);
    }

    @Override
    @WebMethod
    public void updateUserEmail(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "email", partName = "email") @Nullable String email) {
        sessionService.validate(sessionDTO);
        userService.updateEmail(sessionDTO.getUserId(), email);
    }

    @Nullable
    @Override
    @WebMethod
    public void updateUserFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "firstName", partName = "firstName") @Nullable String firstName) {
        sessionService.validate(sessionDTO);
        userService.updateFirstName(sessionDTO.getUserId(), firstName);
    }

    @Override
    @WebMethod
    public void updateUserLastName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "lastName", partName = "lastName") @Nullable String lastName) {
        sessionService.validate(sessionDTO);
        userService.updateLastName(sessionDTO.getUserId(), lastName);
    }

    @Override
    @WebMethod
    public void updateUserMiddleName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "middleName", partName = "middleName") @Nullable String middleName) {
        sessionService.validate(sessionDTO);
        userService.updateMiddleName(sessionDTO.getUserId(), middleName);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO viewUserProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO);
        return sessionService.getUser(sessionDTO);
    }
}
