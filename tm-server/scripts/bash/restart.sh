#!/bin/bash

echo "===> Restart tm-server"
echo ''
if [ ! -f tm-server.pid ]; then
	echo "tm-server is not running!"
	exit 1;
fi

echo 'tm-server was stopped';
kill -9 $(cat tm-server.pid)
rm tm-server.pid

mkdir -p ../bash/log
rm -f ../bash/log/tm-server.log
nohup java -jar ../../tm-server.jar > ../bash/logs/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo 'tm-server is work with pid '$!
